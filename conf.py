
# SET UP THE SIMULATION WITH THESE PARAMS
params = {}
params['chopPoints'] = list(range(16, 53, 4))
params['fasta'] = """>2P6J:A|PDBID|CHAIN|SEQUENCE
MKQWSENVEEKLKEFVKRHQRITQEELHQYAQRLGLNEEAIRQFFEEFEQRK
"""
params['temp'] = 360.0
params['method'] = 'last'  # rmsd, ss, last
params['nanoseconds'] = 100  # nanoseconds per translation setp
params['dt'] = 0.0025
params['cuda'] = '0'
params['reweight'] = False
params['amber16'] = False


testing = False

# DON'T EDIT THIS STUFF UNLESS NEED BE

timefactor = 1
if testing:
    timefactor = 100  # 1 = normal, 100 = testing

params['parmedprogram'] = 'parmed.py'
params['proteinff'] = 'source leaprc.ff14SB'
if params['amber16']:
    params['parmedprogram'] = 'parmed'
    params['proteinff'] = """source leaprc.protein.ff14SB
source leaprc.water.tip3p"""


files = {}
files['01_Min.in'] = """Minimize
 &cntrl
  imin=1,
  ntx=1,
  irest=0,
  maxcyc=%(maxcyc)d,
  ncyc=%(ncyc)d,
  ntpr=%(ntpr)d,
  ntwx=0,
  cut=8.0,
 /
""" % {"maxcyc": 2000 / timefactor, "ncyc": 1000 / timefactor, "ntpr": 100 / timefactor}

files['02_Heat.in'] = """Heat
 &cntrl
  imin=0,
  ntx=1,
  irest=0,
  nstlim=%(nstlim)d,
  dt=%(dt)1.4f,
  ntf=2,
  ntc=2,
  tempi=0.0,
  temp0=%(temp)s,
  ntpr=100,
  ntwx=100,
  cut=8.0,
  ntb=1,
  ntp=0,
  ntt=3,
  gamma_ln=2.0,
  nmropt=1,
  ig=-1,
 /
&wt type='TEMP0', istep1=0, istep2=%(istep2)d, value1=0.0, value2=%(temp)s /
&wt type='TEMP0', istep1=%(istep1)d, istep2=%(nstlim)d, value1=%(temp)s, value2=%(temp)s /
&wt type='END' /
""" % {'temp': str(params['temp']), "nstlim": 10000 / timefactor, "istep2": 9000 / timefactor, "istep1": 1 + 9000 / timefactor, 'dt': params['dt']}


files['025_PreProd.in'] = """Typical Production MD NPT, MC Bar 4fs HMR
 &cntrl
   ntx=5, irest=1,
   ntc=2, ntf=2,
   nstlim=10000,
   ntpr=200, ntwx=500,
   ntwr=200,
   dt=%(dt)1.4f, cut=8.5,
   ntt=1, tautp=10.0,
   temp0=%(temp)d,
   ntb=2, ntp=1, barostat=2,
   ioutfm=1,
 /
""" % {'temp': params['temp'], 'dt': params['dt']}


# files['03_Prod.in'] = """Typical Production MD NPT, MC Bar 4fs HMR
#  &cntrl
#    ntx=5, irest=1,
#    ntc=2, ntf=2,
#    nstlim=4000,
#    ntpr=100, ntwx=100,
#    ntwr=100,
#    dt=%(dt)1.4f, cut=8.5,
#    ntt=1, tautp=10.0,
#    temp0=%(temp)s,
#    ntb=2, ntp=1, barostat=2,
#    ioutfm=1,
#  /
# """ % {'temp':str(params['temp'])}


files['03_Prod.in'] = """Typical Production MD NPT, MC Bar 4fs HMR
 &cntrl
   ntx=5, irest=1,
   ntc=2, ntf=2,
   nstlim=%(time)d,
   ntpr=1000, ntwx=%(ntwx)d,
   ntwr=10000,
   dt=%(dt)1.4f, cut=8.5,
   ntt=1, tautp=10.0,
   temp0=%(temp)d,
   ntb=2, ntp=1, barostat=2,
   ioutfm=1,
 /
""" % {'temp': params['temp'], 'time': params['nanoseconds'] * 400000 / timefactor, 'ntwx': int(params['nanoseconds'] * 400000 / timefactor / 400), 'dt': params['dt']}
