import json
import glob
import subprocess
import shlex
import os
from tqdm import tqdm


def getMinMaxResidues(file1,file2):
    maxResNum = 0
    minResNum = 1000
    with open(file1,'r') as f:
        for line in f:
            if "END" in line or "CRYST" in line or "WAT" in line and "Na" not in line and "Cl" not in line:
                continue
            s = list(line)
            if ''.join(s[0:4]).strip() == "ATOM":
                res = int(''.join(s[23:27]))
                if res > maxResNum:
                    maxResNum = res
                if res < minResNum:
                    minResNum = res


    maxResNum2 = 0
    minResNum2 = 1000
    with open(file2,'r') as f:
        for line in f:
            if "END" in line or "CRYST" in line or "WAT" in line and "Na" not in line and "Cl" not in line:
                continue
            s = list(line)
            if ''.join(s[0:4]).strip() == "ATOM":
                res = int(''.join(s[23:27]))
                if res > maxResNum2:
                    maxResNum2 = res
                if res < minResNum2:
                    minResNum2 = res

    alignMaxRes = maxResNum
    alignMinRes = minResNum
    if maxResNum2 < maxResNum:
        alignMaxRes = maxResNum2-1
    if minResNum2 > minResNum:
        alignMinRes = minResNum2

    return alignMinRes+1, alignMaxRes-2


def alignStructures(file1,file2):
    """Generates new structure based on file2
    that is aligned to file1"""
    alignMinRes, alignMaxRes = getMinMaxResidues(file1,file2)
    with open('temp','w') as f:
        f.write("""mol new %(file1)s
mol new %(file2)s
set sel1 [atomselect 0 "backbone and resid %(res1)s to %(res2)s"]
set sel2 [atomselect 1 "backbone and resid %(res1)s to %(res2)s"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "protein and noh"]
$move_sel move $transformation_matrix
$move_sel writepdb temp.pdb
exit""" % {"file1":file1,"res1":alignMinRes,"res2":alignMaxRes,"file2":file2} )
    cmd = "vmd -dispdev text -e temp"
    proc = subprocess.Popen(shlex.split(cmd),shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    os.remove('temp')
    alignedStructure = open('temp.pdb','r').read()
    os.remove('temp.pdb')
    return alignedStructure


def dumpStructure(structure,model):
    with open("previous.pdb","w") as f:
        for line in structure.split('\n'):
            if "WAT" not in line and "Na" not in line:
                f.write(line + "\n")
    newStructure = ""
    for line in structure.split("\n"):
        if "END" in line or "CRYST" in line or "WAT" in line and "Na" not in line and "Cl" not in line:
            continue
        newStructure = newStructure + line + "\n"
    with open("movie.pdb","a") as f:
        f.write("MODEL %d\n" % model)
        f.write(newStructure)
        f.write("ENDMDL\n")

# Get file list
fileList = []
for i in range(100):
	if os.path.isdir("./%d" % i):
		for j in range(15000):
			if os.path.isfile("./%d/pdbs/%d.pdb" % (i,j)):
				fileList.append("./%d/pdbs/%d.pdb" % (i,j))



try:
    os.remove("movie.pdb")
except:
    pass
try:
    os.remove("previous.pdb")
except:
    pass


dumpStructure(open(fileList[0],"r").read(),1)
for i in tqdm(range(1,len(fileList))):
    dumpStructure(alignStructures("previous.pdb",fileList[i]),i+1)


try:
    os.remove("previous.pdb")
except:
    pass

try:
    os.remove("temp.pdb")
except:
    pass

try:
    os.remove("temp")
except:
    pass

